﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ball : MonoBehaviour {

    public Ball BallPos;
    private Vector3 iniPos;
    public Vector2 movement;
	public float maxy;
	public float maxx;
    public float maxz;
    private Vector3 tmpPos;


	void Awake (){
		iniPos = transform.position;
	}
	
	// Update is called once per frame
	void Update () {
		transform.Translate (movement.x * Time.deltaTime, movement.y * Time.deltaTime, 0);
	}
	void OnTriggerEnter(Collider other){
		switch (other.tag) {
		case "Player":
			changeVelocity (other.gameObject.transform);
           break;
		case "Border":
			movement.x *= -1f;
			break;
		case "Goal":
			movement.x *= -1f;
			break;
		case "Border2":
			movement.y *= -1f;
			break;
		case "Dead":
			tmpPos = new Vector3 (maxx, maxy, maxz);
			transform.position = tmpPos;
			break;
		case "Bloque":
			movement.y *= -1f;
			other.GetComponent<Block> ().Touch ();
			break;
        }

		if (movement.x > 15f) {
			movement.x = 15f;
		}
		if (movement.x < -15f) {
			movement.x = -15f;
		}
		if (movement.y > 15f) {
			movement.y = 15f;
		}
		if (movement.y <- 15f) {
			movement.y = -15f;
		}
	}
	void changeVelocity(Transform Player){
		if (transform.position.x > 0) {
			movement.y *=-0.7f;
		} else {
		movement.y *=-1.2f;
		}

		/*if (transform.position.x >= Player.position){
			
		}
		movement.y *= -1f;*/
	}
}
