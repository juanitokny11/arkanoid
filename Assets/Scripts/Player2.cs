﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player2 : MonoBehaviour {

    public float velx;
    private Vector3 iniPos;
    private float move;
	public Ball ballPos;
    public float maxx;
    private Vector3 tmpPos;

    void Awake(){
        iniPos = transform.position;  
    }
    void Update(){
        move = Input.GetAxis("Horizontal");

        transform.Translate(move * velx * Time.deltaTime, 0 , 0);

        if (transform.position.x >= maxx){
            tmpPos = new Vector3(maxx, transform.position.y,transform.position.z);
            transform.position = tmpPos;
        }
        else if (transform.position.x <= -maxx)
        {
            tmpPos = new Vector3(-maxx, transform.position.y,transform.position.z);
            transform.position = tmpPos;
        }

    }
}
